# Inherit AOSP device configuration for bravo.
$(call inherit-product, device/htc/bravo/full_bravo.mk)

# Inherit some common cyanogenmod stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Include GSM stuff
$(call inherit-product, vendor/cm/config/gsm.mk)

#
# Setup device specific product configuration.
#
PRODUCT_NAME := cm_bravo
PRODUCT_BRAND := htc_wwe
PRODUCT_DEVICE := bravo
PRODUCT_MODEL := HTC Desire
PRODUCT_MANUFACTURER := HTC
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_ID=GRI40 PRODUCT_NAME=htc_bravo BUILD_FINGERPRINT=htc_wwe/htc_bravo/bravo:2.3.3/GRI40/96875.1:user/release-keys TARGET_BUILD_TYPE=userdebug BUILD_VERSION_TAGS=release-keys PRIVATE_BUILD_DESC="3.14.405.1 CL96875 release-keys"

#
# Set up the product codename, build version & MOTD.
#
PRODUCT_CODENAME := Desire
#PRODUCT_VERSION_DEVICE_SPECIFIC := v1
ifeq ($(NIGHTLY_BUILD),true)
    BUILD_VERSION := 1.0.0v4-$(shell date +%m%d%Y)-NIGHTLY
else
    BUILD_VERSION := 1.0.0v4
endif

PRODUCT_PROPERTY_OVERRIDES += \
    ro.build.romversion=MeDroid-$(PRODUCT_CODENAME)-$(BUILD_VERSION)

PRODUCT_MOTD :="\n\n\n--------------------MeDroid---------------------\nGracias Por instalar MeDroidICS en tu htc Desire \nPor favor visitanos en \#Memoriandroid Memoriandroid.com\nsiguenos en twitter @memoriandroide\nsi te gusta nuestro trabajo compranos unas cervezas!\n------------------------------------------------\n"

#
# Extra Packages
#
PRODUCT_PACKAGES += Stk

# Extra RIL settings
PRODUCT_PROPERTY_OVERRIDES += \
    ro.ril.enable.managed.roaming=0 \
    ro.ril.oem.nosim.ecclist=911,112,999,000,08,118,120,122,110,119,995 \
    ro.ril.emc.mode=2

# Release name and versioning
PRODUCT_RELEASE_NAME := Desire
PRODUCT_VERSION_DEVICE_SPECIFIC := -samuaz-$(shell date +%m%d%Y)

PRODUCT_PACKAGES += \
    Camera \
    CMSettings \
    UsbMassStorage \

#Disable visual strict mode, even on eng builds
#PRODUCT_DEFAULT_PROPERTY += persist.sys.strictmode.visual=0
ADDITIONAL_DEFAULT_PROPERTIES += persist.sys.strictmode.override=1

# MeDroid Bootanimation
PRODUCT_COPY_FILES += device/htc/bravo/extras/nexus-bootanimation.zip:system/media/bootanimation.zip

# Get some Gapps
$(call inherit-product-if-exists, gapps/gapps.mk)
